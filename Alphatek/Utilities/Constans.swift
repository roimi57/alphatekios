//
//  Constans.swift
//  Alphatek
//
//  Created by Miguel Ramirez Agustin on 24/11/21.
//

import Foundation
import UIKit

extension UIColor{
    struct AiphatekColor {
        
    static var ButtonGray: UIColor  { return UIColor(displayP3Red: 249/255, green: 249/255, blue: 251/255, alpha: 1.0)}
    static var ButtonBlue: UIColor  { return UIColor(displayP3Red: 29/255, green: 59/255, blue: 240/255, alpha: 1.0)}
    static var ButtonGrayBorder: UIColor  { return UIColor(displayP3Red: 214/255, green: 213/255, blue: 214/255, alpha: 1.0)}
    }
}



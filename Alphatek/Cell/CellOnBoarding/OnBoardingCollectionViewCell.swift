//
//  OnBoardingCollectionViewCell.swift
//  Alphatek
//
//  Created by Miguel Ramirez Agustin on 24/11/21.
//

import UIKit

class OnBoardingCollectionViewCell: UICollectionViewCell {
    @IBOutlet var UILabelTitle: UILabel!
    @IBOutlet var UILabelSutTitle: UILabel!
    @IBOutlet var UiImagenViewOnboarding: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func setOnBoarding(text:String, subTitle:String, img:UIImage){
        self.UILabelTitle.text = text
        self.UILabelSutTitle.text = subTitle
        self.UiImagenViewOnboarding.image = img
    }
    

}

//
//  OnBoardingViewController.swift
//  Alphatek
//
//  Created by Miguel Ramirez Agustin on 24/11/21.
//

import UIKit

enum ProcessOnBoarding {
    case showLogin
}

class OnBoardingViewController: UIViewController {
    
    @IBOutlet var btnViewLocation: UIView!
    @IBOutlet var viewbutton: UIView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var btnDissmis: UIButton!
    @IBOutlet var btnNext: UIButton!
    var arrayImages : [UIImage] = []
    var arrayTitle:[String] = []
    var arraySubTitle:[String] = []
    @IBOutlet var pageControl: UIPageControl!
    
    init(process:ProcessOnBoarding) {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var currentPage = 0 {
        didSet {
            pageControl.currentPage = currentPage
            if currentPage == arrayTitle.count - 1{
                self.pageControl.isHidden = true
                self.viewbutton.isHidden = false
            }else{
                self.viewbutton.isHidden = true
                self.pageControl.isHidden = false
                self.btnDissmis.isHidden = false
            }
        }
     }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.styleComponent()
        self.arrayTitle.append("Cotiza tu reparación")
        self.arrayTitle.append("Agenda tu reparación con un especialista")
        self.arrayTitle.append("¡Listo! Rápido y seguro")
        self.arrayTitle.append("Hola, gusto en concoerte")
        self.arraySubTitle.append("Selecciona el equipo que quieres reparar, cotiza y agenda")
        self.arraySubTitle.append("Selecciona el día, la hora y el lugar donde quieres agendar tu reparación.")
        self.arraySubTitle.append("Espera la confirmación de tu cita y nosotros nos encargaremos de repararlo")
        self.arraySubTitle.append("Escoge tu ubicación para estar en cobertura con nostros")
        self.arrayImages.append( UIImage(named: "onBoarding1")!)
        self.arrayImages.append( UIImage(named: "onBoarding2")!)
        self.arrayImages.append( UIImage(named: "onBoarding3")!)
        self.arrayImages.append( UIImage(named: "onBoarding4")!)
        
        self.collectionView.register(UINib(nibName: "OnBoardingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "OnBoardingCollectionViewCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.pageControl.numberOfPages = self.arrayTitle.count-1
       
    
    }


    func styleComponent(){
        self.btnViewLocation.layer.borderColor = UIColor.AiphatekColor.ButtonBlue.cgColor
        self.btnViewLocation.layer.masksToBounds = true
        self.btnViewLocation.layer.cornerRadius = 4
        self.btnViewLocation.layer.borderWidth = 1.4
        self.btnDissmis.layer.masksToBounds = true
        self.btnDissmis.layer.cornerRadius = 20
        self.btnDissmis.layer.borderWidth = 1
        self.btnDissmis.layer.borderColor = UIColor.AiphatekColor.ButtonGrayBorder.cgColor
        self.btnDissmis.layer.backgroundColor = UIColor.AiphatekColor.ButtonGray.cgColor
        self.btnNext.layer.masksToBounds = true
        self.btnNext.layer.cornerRadius = 20
        self.btnNext.layer.borderColor = UIColor.white.cgColor
    }
    
    // MARK: - Event componet

    
    @IBAction func eventDismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func eventNext(_ sender: Any) {
        if currentPage == arrayTitle.count - 1 {
            dismiss(animated: true) {
            }
        }else{
            currentPage += 1
            let indexPath = IndexPath(item: currentPage, section: 0)
            collectionView.contentSize = CGSize(width: collectionView.bounds.width * CGFloat(arrayTitle.count - 1), height: collectionView.bounds.height)
            collectionView.scrollToItem(at: indexPath, at: .left, animated: true)
        }
    }
    
}


extension OnBoardingViewController:UICollectionViewDelegate{
    
}

extension OnBoardingViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  arrayTitle.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OnBoardingCollectionViewCell", for: indexPath) as? OnBoardingCollectionViewCell
        cell?.setOnBoarding(text: self.arrayTitle[indexPath.row], subTitle: self.arraySubTitle[indexPath.row], img: self.arrayImages[indexPath.row])
        return cell!
    }
}


extension OnBoardingViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
       
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollingFinished(scrollView: scrollView)
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if decelerate {
            return
        }
        scrollingFinished(scrollView: scrollView)
    }

    func scrollingFinished(scrollView: UIScrollView) {
        let width = scrollView.frame.width
        currentPage = Int(scrollView.contentOffset.x / width)
    }
    
}

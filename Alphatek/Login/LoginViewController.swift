//
//  LoginViewController.swift
//  Alphatek
//
//  Created by Miguel Ramirez Agustin on 23/11/21.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet var btnRegister: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnRegister.layer.masksToBounds = true
        self.btnRegister.layer.cornerRadius = 15
        
        Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false){ timer in
            self.showOnBoarding()
        }
        

    }

    
    @IBAction func eventRegister(_ sender: Any) {
        let  _change:ChangePasswordViewController = ChangePasswordViewController()
        if #available(iOS 13.0, *) {
            _change.isModalInPresentation = true
        }
        self.present(_change, animated: true, completion: nil)
    }
    
    func showOnBoarding(){
        print("Muestra pantalla onboarding")
        let _onBoardingVC:OnBoardingViewController = OnBoardingViewController(process: .showLogin)
        if #available(iOS 13.0, *) {
            _onBoardingVC.isModalInPresentation = true
        }
        self.present(_onBoardingVC, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
